﻿using System;
using System.Collections.Generic;

namespace Interview
{
    public class InMemoryRepository<T, TKey> : IRepository<T, TKey> where T : IStoreable<TKey>
    {
        private readonly IDictionary<TKey, T> _dataStore;

        public InMemoryRepository()
        {
            _dataStore = new Dictionary<TKey, T>();
        }

        public InMemoryRepository(IDictionary<TKey, T> dataStore)
        {
            _dataStore = dataStore;
        }

        public void Delete(TKey id)
        {
            if (_dataStore.ContainsKey(id))
            {
                _dataStore.Remove(id);
            }
            else
            {
                throw new ApplicationException($"Unable to delete non-existing entity of {typeof(T).Name}; Id: {id}");
            }
        }

        public T Get(TKey id)
        {
            _dataStore.TryGetValue(id, out var entity);
            return entity;
        }

        public IEnumerable<T> GetAll()
        {
            return _dataStore.Values;
        }

        public void Save(T item)
        {
            if (_dataStore.ContainsKey(item.Id))
            {
                _dataStore.Remove(item.Id, out var removedItem);
                _dataStore.Add(removedItem.Id, item);
            }
            else
            {
                _dataStore.Add(item.Id, item);
            }
        }
    }
}
