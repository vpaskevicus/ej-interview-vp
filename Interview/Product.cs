﻿using System;

namespace Interview
{
    public class Product : IStoreable<int>
    {
        private int _id;

        public Product(int id)
        {
            Id = id;
        }

        public int Id
        {
            get => _id;
            set
            {
                if (value <= 0)
                {
                    throw new ApplicationException(
                        $"Unable to create an instance of {nameof(Product)}; {nameof(Product.Id)}: {value}");
                }

                _id = value;
            }
        }
    }
}
