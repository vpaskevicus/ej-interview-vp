using System;
using FluentAssertions;
using Xunit;

namespace Interview.UnitTests
{
    public class ProductShould
    {
        [Fact]
        public void Create_An_Instance_With_Given_Id()
        {
            const int productId = 1;

            var product = new Product(productId);

            product.Id.Should().Be(productId);
        }

        [Theory]
        [InlineData(-2)]
        [InlineData(0)]
        public void Throw_Exception_If_Id_Is_Invalid(int id)
        {
            Action action = () => new Product(id);

            action.Should().Throw<ApplicationException>()
                .WithMessage($"Unable to create an instance of {nameof(Product)}; {nameof(Product.Id)}: {id}");
        }
    }
}
