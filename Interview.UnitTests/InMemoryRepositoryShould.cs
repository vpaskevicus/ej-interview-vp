﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Interview.UnitTests
{
    public class InMemoryRepositoryShould
    {
        private readonly IDictionary<int, Product> _defaultProductDataStore;
        private readonly Product _firstProduct;
        private readonly Product _secondProduct;


        public InMemoryRepositoryShould()
        {
            _firstProduct = new Product(1);
            _secondProduct = new Product(2);

            _defaultProductDataStore = new Dictionary<int, Product>
                {{_firstProduct.Id, _firstProduct}, {_secondProduct.Id, _secondProduct}};
        }

        [Fact]
        public void Return_Empty_Result_If_Data_Store_Is_Empty()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>();

            var result = inMemoryRepository.GetAll();

            result.Should().BeEmpty();
        }

        [Fact]
        public void Return_All_Entities_From_Data_Store()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>(_defaultProductDataStore);

            var result = inMemoryRepository.GetAll().ToList();

            result.Should().NotBeEmpty();
            result.Count.Should().Be(2);
            result.ElementAt(0).Id.Should().Be(_firstProduct.Id);
            result.ElementAt(1).Id.Should().Be(_secondProduct.Id);
        }

        [Fact]
        public void Return_Entity_Given_Id()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>(_defaultProductDataStore);

            var result = inMemoryRepository.Get(_firstProduct.Id);

            result.Should().NotBeNull();
            result.Id.Should().Be(_firstProduct.Id);
        }

        [Fact]
        public void Return_Null_If_Entity_Does_Not_Exist()
        {
            const int nonExistingId = 5;

            var inMemoryRepository = new InMemoryRepository<Product,int>(_defaultProductDataStore);

            var result = inMemoryRepository.Get(nonExistingId);

            result.Should().BeNull();
        }

        [Fact]
        public void Save_Entity_Into_Data_Store()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>();

            inMemoryRepository.Save(_firstProduct);

            var result = inMemoryRepository.GetAll().ToList();

            result.Should().NotBeEmpty();
            result.Count.Should().Be(1);
            result.ElementAt(0).Id.Should().Be(_firstProduct.Id);
        }

        [Fact]
        public void Update_Existing_Entity_In_Data_Store()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>();

            inMemoryRepository.Save(_firstProduct);
            inMemoryRepository.Save(_firstProduct);
        }

        [Fact]
        public void Delete_Existing_Entity_In_Data_Store()
        {
            var inMemoryRepository = new InMemoryRepository<Product, int>(_defaultProductDataStore);

            inMemoryRepository.Delete(_firstProduct.Id);

            var result = inMemoryRepository.GetAll().ToList();

            result.Should().NotBeNull();
            result.Count.Should().Be(1);
            result.ElementAt(0).Id.Should().Be(_secondProduct.Id);
        }

        [Fact]
        public void Throw_Exception_If_Trying_To_Remove_Non_Existing_Entity()
        {
            const int nonExistingId = 5;

            var inMemoryRepository = new InMemoryRepository<Product, int>();

            Action action = () => inMemoryRepository.Delete(nonExistingId);

            action.Should().Throw<ApplicationException>().WithMessage(
                $"Unable to delete non-existing entity of {nameof(Product)}; {nameof(Product.Id)}: {nonExistingId}");
        }
    }
}
